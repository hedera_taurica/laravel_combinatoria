<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('/combinatoria', function() {
    // modo independiente, todos en localhost
    // $colorResponse = Http::get('http://localhost/laravel_colores/public/api/colores');
    // $animalResponse = Http::get('http://localhost/laravel_animales/public/api/animales');

    // modo independiente, todos en docker
    // $colorResponse = Http::get('http://172.17.0.4:8000/api/colores');
    // $animalResponse = Http::get('http://172.17.0.3:8000/api/animales');

    // Configurar el tiempo de espera en segundos
    $timeout = 3;

    // Realizar la solicitud HTTP para obtener el color
    try {
        $colorResponse = Http::timeout($timeout)->get('http://laravel_colores:8000/api/colores');
        $color = $colorResponse->json()['color'];
    } catch (\Exception $e) {
        // Si hay un error o se supera el tiempo de espera, devolver un valor por defecto
        $color = 's/n';
    }

    // Realizar la solicitud HTTP para obtener el animal
    try {
        $animalResponse = Http::timeout($timeout)->get('http://laravel_animales:8000/api/animales');
        $animal = $animalResponse->json()['animal'];
    } catch (\Exception $e) {
        // Si hay un error o se supera el tiempo de espera, devolver un valor por defecto
        $animal = 's/n';
    }

    // Combinar los resultados
    $combinacion = "$animal-$color";

    // Generar un log aleatorio
    $logLevels = ['info', 'warning', 'error', 'debug'];
    $randomLogLevel = $logLevels[array_rand($logLevels)];
    Log::{$randomLogLevel}('Combinación seleccionada: ' . $combinacion);

    // Devolver la respuesta en formato JSON
    return response()->json(['combinacion' => $combinacion]);
});
